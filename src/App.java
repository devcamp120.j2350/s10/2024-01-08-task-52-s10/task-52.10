public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World! Sum: " + App.sumNumbersV1());

        int[] myArray1 = new int[]{1, 5, 10};
        System.out.println("Sum: {1, 5, 10} :" + App.sumNumbersV2(myArray1));

        int[] myArray2 = new int[]{1,2,3,5,7,9};
        
        System.out.println("Sum: {1,2,3,5,7,9} :" + App.sumNumbersV2(myArray2));
        int[] myArray3 = myArray2;
        myArray2[0]=5;
        System.out.println("Sum: {5,2,3,5,7,9} :" + App.sumNumbersV2(myArray3));

        App.printHello(22);
        App.printHello(99);
    }

    public static int sumNumbersV1() {
        int sum = 0;

        for (int i=1; i<=100;i++) {
            sum += i;
        }

        return sum;
    }

    public static int sumNumbersV2(int[] myArrayLists) {
        int sum = 0;

        for (int number: myArrayLists) {
             sum += number;
        }

        return sum;
    }

    public static void printHello(int number) {
        if (number%2==0) {
            System.out.println(number + " - Day la so chan");
        } else {
            System.out.println(number + " - Day la so le");
        }
    }
}
